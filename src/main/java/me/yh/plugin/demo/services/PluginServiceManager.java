package me.yh.plugin.demo.services;

/**
 * Created by zhouyanhui on 16-5-28.
 */
public interface PluginServiceManager {
	/**
	 * 	新增
	 * @param jarUri url地址 jar:file:/E:/git/dynamic-load/external/jar/fastjson.jar!/
	 * @param pluginName 插件名称 
	 * @param service 服务名称
	 */
      void onNew(String jarUri, String pluginName, String service);
	/**
	 * 	修改
	 * @param jarUri url地址 jar:file:/E:/git/dynamic-load/external/jar/fastjson.jar!/
	 * @param pluginName 插件名称 
	 * @param service 服务名称
	 */
      void onChange(String jarUri, String pluginName, String service);
      /**
       * 	删除
       * @param pluginName 插件名称 
       * @param service 服务名称
       */
      void onRemove(String pluginName, String service);
      /**
       * 	获取服务信息
       * @param serviceName 服务名称
       * @return
       */
      public ServiceInstance getService(String serviceName);

}
