package me.yh.tech.dynamic;


import java.util.HashMap;
import java.util.Map;

import me.yh.tech.util.compiler.JavaStringCompiler;


/**
 * 动态代码没有实现接口, 直接反射调用方法
 * 
 * 注: 此处为练习使用, 建议给动态代码定义接口
 * 动态加载 jar 第三方
 * @author liwei
 *
 */
public class App3 {

	private static final String SOURCE_CODE = "package tech.lw.dynamic.service.impl;"
			+ "import java.util.ArrayList;"
			+ "import java.util.List;"
			+ "import com.alibaba.fastjson.JSONObject;"
			+ "public class ServiceImpl2 {"
			+ "private List list = new ArrayList();"
			+ "public ServiceImpl2(){}"
			+ "public void start(String name) {"
			+ "Map map = new HashMap();"
			+ "map.put(\"1\",\"v\");"
			+ "JSONObject.toJSONString(map);"
			+ "System.out.println(\"--- 开启服务 ---\"+name);"
			+ "}"
			+ "public void stop() {"
			+ "System.out.println(\"--- 停止服务 ---\");"
			+ "}"
			+ "}";

    public static void main(String[] args) {
    	JavaStringCompiler compiler = new JavaStringCompiler();  // 创建动态编译器
    	
    	try {
    		// 动态编译 Java 文件, 生成字节码
        	Map<String, byte[]> result = compiler.compile("ServiceImpl2.java", SOURCE_CODE);

			System.out.println(SOURCE_CODE);

			Class<?> clazz = compiler.loadClass("tech.lw.dynamic.service.impl.ServiceImpl2", result);
        	
        	Object obj = clazz.newInstance();  // 使用反射创建对象
        	
        	clazz.getMethod("start",String.class).invoke(obj,"liwei");  // 调用 ServiceImpl2 有参的 start 方法
        	//clazz.getMethod("start").invoke(obj,new Object[] {"liwei"});  // 调用 ServiceImpl2 无参的 start 方法
        	clazz.getMethod("stop").invoke(obj);  // 调用 ServiceImpl2 无参的 stop 方法
        	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
    }
}
